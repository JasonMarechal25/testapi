
#pragma once

namespace API {

    class Collaborator {

    public:
        void printMe() const;
    };

} // API
