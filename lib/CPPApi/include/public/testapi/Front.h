
#pragma once

#include <string>
#include "Collaborator.h"

namespace API {

    class Front {
    public:
        Front(std::string name, int n);

        void printIt(const Collaborator& collaborator) const;

    private:
        std::string name_;
        int n_;
    };

} // API
