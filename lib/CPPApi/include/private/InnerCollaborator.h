
#pragma once

#include <string>

namespace API {

    class InnerCollaborator {
    public:
        [[nodiscard]] std::string name() const noexcept;
    };

} // API
