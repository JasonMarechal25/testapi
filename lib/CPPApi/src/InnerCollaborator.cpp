
#include "InnerCollaborator.h"

namespace API {
    std::string InnerCollaborator::name() const noexcept {
        return "inner";
    }
} // API