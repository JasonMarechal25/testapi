
#include "testapi/Front.h"

#include <utility>

namespace API {
    void Front::printIt(const Collaborator &collaborator) const {
        collaborator.printMe();
    }

    Front::Front(std::string name, int n):
    name_(std::move(name)),
    n_(n)
    {

    }
} // API