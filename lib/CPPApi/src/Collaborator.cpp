
#include "testapi/Collaborator.h"
#include <iostream>
#include "../include/private/InnerCollaborator.h"

namespace API {
    void Collaborator::printMe() const {
    InnerCollaborator inner;
    std::cout << "I'm here!" << inner.name() << std::endl;
    }
} // API