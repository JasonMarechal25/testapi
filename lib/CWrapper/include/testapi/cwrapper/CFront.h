//Not pragma once since it's for a C API, just in case
#ifndef C_FRONT_INCLUDE
#define C_FRONT_INCLUDE
#include "CCollaborator.h"

#ifdef __cplusplus
extern "C"
{
#endif
struct HFront; // An opaque type that we'll use as a handle
typedef struct HFront HFront;
HFront * front_create( const char * name, int n );
void front_destroy( HFront * v );
void front_printIt( HFront * v, HCollaborator* c );
#ifdef __cplusplus
}
#endif
#endif