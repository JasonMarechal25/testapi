//Not pragma once since it's for a C API, just in case
#ifndef C_COLLABORATOR_INCLUDE
#define C_COLLABORATOR_INCLUDE

#ifdef __cplusplus
extern "C"
{
#endif
struct HCollaborator; // An opaque type that we'll use as a handle
typedef struct HCollaborator HCollaborator;
HCollaborator * collaborator_create();
void collaborator_destroy( HCollaborator * v );
void collaborator_printMe( HCollaborator * v);
#ifdef __cplusplus
}
#endif
#endif