#include <cstddef>
#include "testapi/Front.h"
#include "testapi/cwrapper/CFront.h"
using namespace API;

#ifdef __cplusplus
extern "C"
{
#endif
HFront * front_create( const char * name, int n ){
    try {
        return reinterpret_cast<HFront*>(new Front(name, n));
    } catch ( ... ) {
        return NULL;
    }
}
void front_destroy( HFront * v ){
    try {
        delete reinterpret_cast<Front*>(v);
    } catch ( ... ) {

    }
}
void front_printIt( HFront * v, HCollaborator* c ){
    try {
        return reinterpret_cast<Front*>(v)->printIt(*reinterpret_cast<Collaborator*>(c));
    } catch ( ... ) {

    }
}

#ifdef __cplusplus
}
#endif