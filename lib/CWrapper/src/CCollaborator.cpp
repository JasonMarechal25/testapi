#include <cstddef>
#include "testapi/Collaborator.h"

using namespace API;

#include "testapi/cwrapper/CCollaborator.h"
#ifdef __cplusplus
extern "C"
{
#endif

HCollaborator * collaborator_create(){
    try {
        return reinterpret_cast<HCollaborator*>(new Collaborator());
    } catch ( ... ) {
        return NULL;
    }
}
void collaborator_destroy( HCollaborator * v ){
    try {
        delete reinterpret_cast<HCollaborator*>(v);
    } catch ( ... ) {

    }
}
void collaborator_printMe( HCollaborator * v){
    try {
        return reinterpret_cast<Collaborator*>(v)->printMe();
    } catch ( ... ) {

    }
}
#ifdef __cplusplus
}
#endif

