add_library(CFront)
add_library(TestApi::CFront ALIAS CFront)

target_sources(CFront
PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include/testapi/cwrapper/CFront.h>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include/testapi/cwrapper/CCollaborator.h>
PRIVATE
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src/CFront.cpp>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src/CCollaborator.cpp>
)

target_link_libraries(CFront
        PRIVATE
        TestApi::TestApi
        stdc++ #Force link to stdc++ to resolve undefined symbols for C clients
)

target_include_directories(CFront
PUBLIC
$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
)

install(DIRECTORY include/testapi/cwrapper
        DESTINATION "include"
)