install(EXPORT TestApiTargets
        FILE TestApiTargets.cmake
        NAMESPACE TestApi::
        DESTINATION lib/cmake/TestApi
)

configure_file(TestApiConfig.cmake.in TestApiConfig.cmake @ONLY)
install(FILES "${CMAKE_CURRENT_BINARY_DIR}/TestApiConfig.cmake"
        "${CMAKE_CURRENT_BINARY_DIR}/TestApiConfigVersion.cmake"
        DESTINATION lib/cmake/TestApi
)