#include <testapi/Front.h>
#include <testapi/Collaborator.h>
#include <string>

int main() {
    using namespace std::string_literals;
    API::Collaborator c;
    API::Front f("plop"s, 4);
    f.printIt(c);
    return 0;
}
