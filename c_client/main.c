#include <testapi/CFront.h>
#include <testapi/CCollaborator.h>


int main() {
    HFront* cfront = front_create("plop", 5);
    HCollaborator* ccoll = collaborator_create();
    front_printIt(cfront, ccoll);
    return 0;
}
